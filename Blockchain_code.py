import datetime 

# Calculating the hash

# in order to add digital

# fingerprints to the blocks

import hashlib 

# To store data

# in our blockchain

import json 

# Flask is for creating the web

# app and jsonify is for

# displaying the blockchain

from flask import Flask, jsonify 

class Blockchain: 
    # This function is created 

    # to create the very first 

    # block and set it's hash to "0" 
    def _init_(self): 
        self.chain = [] 
        self.create_block(proof=1, previous_hash='0') 

    # This function is created 

    # to add further blocks 

    # into the chain 

    def create_block(self, proof, previous_hash): 

        block = {'index': len(self.chain) + 1, 'timestamp': str(datetime.datetime.now()), 'proof': proof, 'previous_hash': previous_hash} 
     
        self.chain.append(block) 

        return block 

    # This function is created 

    # to display the previous block 

    def print_previous_block(self): 

        return self.chain[-1] 
    
    # This is the function for proof of work 

    # and used to successfully mine the block 

    def proof_of_work(self, previous_proof): 

        new_proof = 1 

        check_proof = False 


        while check_proof is False: 
            hash_operation = hashlib.sha256(str(new_proof*2 - previous_proof*2).encode()).hexdigest() 

            if hash_operation[:4] == '00000': 
                check_proof = True 

            else: 
                new_proof
